﻿using BookStore.DataAccess.Models;
using System;
using System.Collections.Generic;

namespace BookStore.DataAccess.Helpers
{
    public class MockContext
    {
        private static MockContext mockContext;
        
        private static object syncRoot = new Object(); 
        public List<Guest> Guests { get; }
        public List<User> Users { get; }
        public List<Order> Orders { get; }
        public List<Admininstrator> Admininstrators { get; }
        public List<Product> Products { get; }
        public List<OrderProduct> OrderProducts { get; }
       
        public MockContext()
        {
            Guests = new List<Guest>();
            Users = new List<User>();
            Orders = new List<Order>();
            Admininstrators = new List<Admininstrator>();
            Products = new List<Product>();
            OrderProducts = new List<OrderProduct>();
        }
        public static MockContext GetInstance()
        {
            if (mockContext == null)
            {
                lock (syncRoot)
                {
                    if (mockContext == null)
                        mockContext = new MockContext();
                }
            }
            return mockContext;
        }
    }
}
