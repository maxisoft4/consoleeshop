﻿using BookStore.DataAccess.Helpers;
using BookStore.DataAccess.Models;
using BookStore.DataAccess.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookStore.DataAccess.Repositories
{
    public class UserDataAccess : IBaseRepositoryDataAccess, IUserRepositoryDataAccess
    {
        private readonly MockContext _mockContext;
        public UserDataAccess()
        {
            _mockContext = MockContext.GetInstance();
        }
        public bool CancelOrder(string id)
        {

            if (_mockContext.Orders.First(o => o.Id.ToString() == id) != null)
            {
                _mockContext.Orders.First(o => o.Id.ToString() == id).Status = Status.UserCanceled;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void CreateOrder(Product product)
        {
            if (product == null)
            {
                throw new ArgumentNullException($"{product} cannot be null");
            }
            var user = _mockContext.Users.First(u => u.IsInStore);
            _mockContext.Orders.Add(new Order
            {
                Product = product,
                Status = Status.New,
                User = user
            });
        }

        public void EditPersonalData(string username, string password, string confirnPassword)
        {
            var user = _mockContext.Users.First(u => u.IsInStore);
            if (user != null)
            {
                user.UserName = username;
                user.Password = password;
                user.ConfirmPassword = confirnPassword;
            }
        }

        public void Exit()
        {
            Environment.Exit(0);
        }

        public OrderProduct FillOrder(string id, DateTime date)
        {
            var newOrder = _mockContext.Orders.Find(a => a.Id.ToString() == id);
            var orderProduct = new OrderProduct();
            newOrder.Status = Status.New;
            newOrder.Status = Status.Paid;
            newOrder.Status = Status.Sent;
            orderProduct.Product = newOrder.Product;
            orderProduct.OrderDate = date;
            orderProduct.Order = newOrder;
            _mockContext.OrderProducts.Add(orderProduct);
            return orderProduct;
        }

        public Product SearchProduct(string name)
        {
            return _mockContext.Products.Find(p => p.Name == name);
        }

        public bool SetStatusRecieved(string id)
        {
            if (_mockContext.Orders.Find(o => o.Id.ToString() == id) != null)
            {
                _mockContext.Orders.Find(o => o.Id.ToString() == id).Status = Status.Recieved;
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Product> ViewAllProducts()
        {
            return _mockContext.Products;
        }

        public List<OrderProduct> ViewHistory()
        {
            return _mockContext.OrderProducts;
        }
    }
}
