﻿using BookStore.DataAccess.Helpers;
using BookStore.DataAccess.Models;
using BookStore.DataAccess.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DataAccess.DataAccess
{
    public class BaseDataAccess : IBaseRepositoryDataAccess
    {
        private readonly MockContext _mockContext;
        public BaseDataAccess()
        {
            _mockContext = MockContext.GetInstance();
        }
        public Product SearchProduct(string name)
        {
            return _mockContext.Products.Find(item => item.Name == name);
        }
        public List<Product> ViewAllProducts()
        {
            return _mockContext.Products;
        }
    }
}
