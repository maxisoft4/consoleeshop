﻿using BookStore.DataAccess.Models;
using System;
using System.Collections.Generic;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    interface IAdministratorDataAccess
    {
        void Exit();
        void CreateOrder(Product product);
        OrderProduct FillOrder(string id, int bankcard, DateTime date);
        bool CancelOrder(string id);
        List<User> ViewUsersData();
        User ViewUserData(string id); 
        bool EditProductCategory(string id, CategoryProduct category);
        bool EditProductDescription(string id, string description);
        bool EditProductPrice(string id, decimal price);
        void CreateProduct(string name, CategoryProduct category, string description, decimal price);
        bool EditOrderStatus(string id, Status status);
        bool EditOrderDate(string id, DateTime date);
    }
}
