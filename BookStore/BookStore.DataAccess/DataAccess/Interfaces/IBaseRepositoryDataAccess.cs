﻿using BookStore.DataAccess.Models;
using System.Collections.Generic;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    interface IBaseRepositoryDataAccess
    {
        List<Product> ViewAllProducts();
        Product SearchProduct(string name);
    }
}
