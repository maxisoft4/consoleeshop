﻿
namespace BookStore.DataAccess.Repositories.Interfaces
{
    interface IGuestRepositoryDataAccess
    {
        bool SignIn(string name,string password);
        void RegisterUser(string email, string name, string password, string confirnPassword);
        void RegisterAdmin(string name, string password, string confirnPassword);
    }
}
