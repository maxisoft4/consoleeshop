﻿using BookStore.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    interface IUserRepositoryDataAccess
    {
        void Exit();
        void CreateOrder(Product product);
        OrderProduct FillOrder(string id, DateTime date);
        bool CancelOrder(string id);
        List<OrderProduct> ViewHistory();
        bool SetStatusRecieved(string id);
        void EditPersonalData(string username, string password, string confirnPassword);
    }
}
