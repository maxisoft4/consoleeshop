﻿using BookStore.DataAccess.Helpers;
using BookStore.DataAccess.Models;
using BookStore.DataAccess.Repositories.Interfaces;
using Moq;
using System.Collections.Generic;

namespace BookStore.DataAccess.Repositories
{
    public class GuestDataAccess : IBaseRepositoryDataAccess, IGuestRepositoryDataAccess
    {
        private readonly MockContext _mockContext;
        public GuestDataAccess()
        {
            _mockContext = MockContext.GetInstance();
        }
        public void RegisterUser(string email, string name, string password, string confirnPassword)
        {
            var user = new User()
            {
                Email = email,
                UserName = name,
                Password = password,
                ConfirmPassword = confirnPassword,
                IsInStore = true
            };
            _mockContext.Users.Add(user);
        }

        public void RegisterAdmin(string name, string password, string confirnPassword)
        {
            var admin = new Admininstrator
            {
                Name = name,
                Password = password,
                ConfirmPassword = confirnPassword,
                IsInStore = true
            };
            _mockContext.Admininstrators.Add(admin);
        }

        public Product SearchProduct(string name)
        {
            return _mockContext.Products.Find(p => p.Name == name); 
        }

        public bool SignIn(string name, string password)
        {
            if(_mockContext.Admininstrators.Contains(_mockContext.Admininstrators.Find(a=>a.Name == name && a.Password == password)) || _mockContext.Users.Contains(_mockContext.Users.Find(a => a.UserName == name && a.Password == password))){
                return true;
            }
            else
            {
                return false;
            }
        }
        public List<Product> ViewAllProducts()
        {
            return _mockContext.Products;
        }
    }
}

