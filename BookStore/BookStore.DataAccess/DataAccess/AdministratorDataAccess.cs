﻿using BookStore.DataAccess.Helpers;
using BookStore.DataAccess.Models;
using BookStore.DataAccess.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookStore.DataAccess.Repositories
{
    public class AdministratorDataAccess : IBaseRepositoryDataAccess, IAdministratorDataAccess
    {
        private readonly MockContext _mockContext;
        public AdministratorDataAccess()
        {
            _mockContext = MockContext.GetInstance();
        }

        public bool CancelOrder(string id)
        {

            if (_mockContext.Orders.First(o => o.Id.ToString() == id) != null)
            {
                _mockContext.Orders.First(o => o.Id.ToString() == id).Status = Status.AdminCanceled;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void CreateOrder(Product product)
        {
            if (product == null)
            {
                throw new ArgumentNullException($"{product} cannot be null");
            }
            _mockContext.Orders.Add(new Order
            {
                Product = product,
                Status = Status.New
            });
        }

        public void CreateProduct(string name, CategoryProduct category, string description, decimal price)
        {
            _mockContext.Products.Add(new Product
            {
                Name = name,
                CategoryProduct = category,
                Description = description,
                Price = price
            });
        }

        public bool EditOrderStatus(string id, Status status)
        {
            var editOrder = _mockContext.Orders.First(o => o.Id.ToString() == id);
            if (editOrder != null)
            {
                editOrder.Status = status;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool EditOrderDate(string id, DateTime date)
        {
            var editOrderProduct = _mockContext.OrderProducts.First(o => o.Id.ToString() == id);
            if (editOrderProduct != null)
            {
                editOrderProduct.OrderDate = date;
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool EditProductCategory(string id, CategoryProduct category)
        {
            var editProduct = _mockContext.Products.First(p => p.Id.ToString() == id);
            if (editProduct != null)
            {
                editProduct.CategoryProduct = category;
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool EditProductDescription(string id, string description)
        {
            var editProduct = _mockContext.Products.First(p => p.Id.ToString() == id);
            if (editProduct != null)
            {
                editProduct.Description = description;
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool EditProductPrice(string id, decimal price)
        {
            var editProduct = _mockContext.Products.First(p => p.Id.ToString() == id);
            if (editProduct != null)
            {
                editProduct.Price = price;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Exit()
        {
            Environment.Exit(0);
        }

        public OrderProduct FillOrder(string id, int bankcard, DateTime date)
        {
            var newOrder = _mockContext.Orders.Find(a => a.Id.ToString() == id);
            var filledOrder = new OrderProduct();
            newOrder.Status = Status.Paid;
            filledOrder.BankCard = bankcard;
            filledOrder.Order = newOrder;
            filledOrder.OrderDate = date;
            filledOrder.Product = newOrder.Product;
            _mockContext.OrderProducts.Add(filledOrder);
            return filledOrder;
        }

        public Product SearchProduct(string name)
        {
            return _mockContext.Products.Find(p => p.Name == name);
        }

        public List<Product> ViewAllProducts()
        {
            return _mockContext.Products;
        }

        public User ViewUserData(string id)
        {
            var user = _mockContext.Users.Find(u => u.Id.ToString() == id);
            return user;
        }

        public List<User> ViewUsersData()
        {
            var users = _mockContext.Users;
            return users;
        }
    }
}
