﻿using System;

namespace BookStore.DataAccess.Models.Abstractions
{
    public class BaseEntity
    {   
        public Guid Id { get; set; }
    }
}
