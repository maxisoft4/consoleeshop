﻿using BookStore.DataAccess.Models.Abstractions;
using System.ComponentModel.DataAnnotations;

namespace BookStore.DataAccess.Models
{
    public class Admininstrator : BaseEntity
    {
        public string Name { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [StringLength(16, MinimumLength = 6, ErrorMessage = "Invalid password length minimum 6 symbol")]
        [RegularExpression(@"[a–z]\[0-9]\S$", ErrorMessage = "Administrator name must include only a-z symbols")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Passwords are not the same")]
        [Compare("Password", ErrorMessage = "Passwords are not the same")]
        public string ConfirmPassword { get; set; }
        public bool IsInStore { get; set; }

    }
}
