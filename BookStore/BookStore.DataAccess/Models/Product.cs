﻿using BookStore.DataAccess.Models.Abstractions;

namespace BookStore.DataAccess.Models
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public CategoryProduct CategoryProduct { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}
