﻿using BookStore.DataAccess.Models.Abstractions;

namespace BookStore.DataAccess.Models
{
    public class Order : BaseEntity
    {
        public Product Product { get; set; }
        public User User { get; set; }
        public Status Status { get; set; }

    }
}
