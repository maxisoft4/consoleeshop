﻿using BookStore.DataAccess.Models.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DataAccess.Models
{
    public class OrderProduct : BaseEntity
    {
        public Product Product { get; set; }
        public Order Order { get; set; }
        public int BankCard { get; set; }
        public DateTime OrderDate { get; set; }

    }
}
