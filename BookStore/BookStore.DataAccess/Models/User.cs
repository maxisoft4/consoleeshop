﻿using BookStore.DataAccess.Models.Abstractions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookStore.DataAccess.Models
{
    public class User : BaseEntity
    {
        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Wrong email address")]
        public string Email { get; set; }
        [Required(ErrorMessage = "UserName is required")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Invalid name length minimum 3 symbol")]
        [RegularExpression(@"[a–z]\[^0-9]\char\S$", ErrorMessage = "Username must include only a-z symbols")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [StringLength(16, MinimumLength = 6, ErrorMessage = "Invalid password length minimum 6 symbol")]
        [RegularExpression(@"[a–z]\[0-9]\S$", ErrorMessage = "Username must include only a-z symbols")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Passwords are not the same")]
        [Compare("Password", ErrorMessage = "Passwords are not the same")]
        public string ConfirmPassword { get; set; }
        public bool IsInStore { get; set; }
        public ICollection<Order> Orders { get; set; }

    }
}
