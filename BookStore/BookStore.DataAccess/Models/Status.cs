﻿
namespace BookStore.DataAccess.Models
{
    public enum Status
    {
        New = 0,
        AdminCanceled = 1,
        UserCanceled = 2,
        Paid = 3,
        Sent = 4,
        Recieved = 5,
        Finished = 6
    }
}
