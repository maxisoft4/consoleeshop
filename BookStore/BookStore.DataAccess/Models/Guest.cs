﻿using BookStore.DataAccess.Models.Abstractions;

namespace BookStore.DataAccess.Models
{
    public class Guest : BaseEntity
    {
        public bool IsInStore { get; set; }
    }
}
