using BookStore.DataAccess.Helpers;
using BookStore.DataAccess.Models;
using System;
using Xunit;

namespace BookStore.XTest
{
    public class RegisterTests
    {
        [Fact]
        public void AddUser()
        {
            var mockContext = new MockContext();
            var user = new User
            {
                UserName = "Username1",
                Email = "useremail@mail.com",
                Password = "123456",
                ConfirmPassword = "123456"
            };
            mockContext.Users.Add(user);
            var expected = mockContext.Users.Contains(user);
            var actual = mockContext.Users.Contains(user);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void AddAdmin()
        {
            var mockContext = new MockContext();
            var admin = new Admininstrator
            {
                Name = "AdminName",
                Password = "123456",
                ConfirmPassword = "123456"
            };
            mockContext.Admininstrators.Add(admin);
            var expected = mockContext.Admininstrators.Contains(admin);
            var actual = mockContext.Admininstrators.Contains(admin);

            Assert.Equal(expected, actual);
        }
    }
}
