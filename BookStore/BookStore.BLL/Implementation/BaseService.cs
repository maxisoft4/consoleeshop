﻿using BookStore.DataAccess.DataAccess;
using BookStore.DataAccess.Models;
using BookStore.DataAccess.Repositories;
using BookStore.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Services.Implementation
{
    public class BaseService : IBaseService
    {
        private readonly BaseDataAccess _baseDataAccess;

        public BaseService()
        {
            _baseDataAccess = new BaseDataAccess();
        }

        public Product SearchProduct(string name)
        {
            return _baseDataAccess.SearchProduct(name);
        }

        public List<Product> ViewAllProducts()
        {
            return _baseDataAccess.ViewAllProducts();
        }
    }
}
