﻿using BookStore.DataAccess.Models;
using BookStore.DataAccess.Repositories;
using BookStore.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Services.Implementation
{
    public class UserService : IUserService, IBaseService
    {

        private readonly UserDataAccess _userDataAccess;

        public UserService()
        {
            _userDataAccess = new UserDataAccess();
        }

        public bool CancelOrder(string id)
        {
            if(_userDataAccess.CancelOrder(id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void CreateOrder(Product product)
        {
            _userDataAccess.CreateOrder(product);
        }

        public void EditPersonalData(string username, string password, string confirnPassword)
        {
            _userDataAccess.EditPersonalData(username, password, confirnPassword);
        }

        public void Exit()
        {
            _userDataAccess.Exit();
        }

        public OrderProduct FillOrder(string id, DateTime date)
        {
            return _userDataAccess.FillOrder(id, date);
        }

        public Product SearchProduct(string name)
        {
            throw new NotImplementedException();
        }

        public bool SetStatusRecieved(string id)
        {
            if (_userDataAccess.SetStatusRecieved(id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Product> ViewAllProducts()
        {
            return _userDataAccess.ViewAllProducts();
        }

        public List<OrderProduct> ViewHistory()
        {
            return _userDataAccess.ViewHistory();
        }
    }
}
