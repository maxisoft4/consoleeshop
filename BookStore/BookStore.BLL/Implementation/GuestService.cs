﻿using BookStore.DataAccess.Models;
using BookStore.DataAccess.Repositories;
using BookStore.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace BookStore.Services.Implementation
{
    public class GuestService : IGuestService, IBaseService
    {
        private readonly GuestDataAccess _guestDataAccess;

        public GuestService()
        {
            _guestDataAccess = new GuestDataAccess();
        }

        public void RegisterAdmin(string name, string password, string confirnPassword)
        {
            _guestDataAccess.RegisterAdmin(name, password, confirnPassword);
        }

        public void RegisterUser(string email, string name, string password, string confirnPassword)
        {
            _guestDataAccess.RegisterUser(email, name, password, confirnPassword);
        }

        public Product SearchProduct(string name)
        {
            throw new NotImplementedException();
        }

        public bool SignIn(string name, string password)
        {
            if(_guestDataAccess.SignIn(name, password))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Product> ViewAllProducts()
        {
            return _guestDataAccess.ViewAllProducts();
        }
    }
}
