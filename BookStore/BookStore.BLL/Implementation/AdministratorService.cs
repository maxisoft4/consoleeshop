﻿using BookStore.DataAccess.Models;
using BookStore.DataAccess.Repositories;
using BookStore.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace BookStore.Services.Implementation
{
    public class AdministratorService : IAdministatorServices, IBaseService
    {

        private readonly AdministratorDataAccess _administratorDataAccess;

        public AdministratorService()
        {
            _administratorDataAccess = new AdministratorDataAccess();
        }

        public bool CancelOrder(string id)
        {
            if(_administratorDataAccess.CancelOrder(id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void CreateOrder(Product product)
        {
            _administratorDataAccess.CreateOrder(product);
        }

        public void CreateProduct(string name, CategoryProduct category, string description, decimal price)
        {
            _administratorDataAccess.CreateProduct(name, category, description, price);
        }

        public bool EditOrderDate(string id, DateTime date)
        {
            if (_administratorDataAccess.EditOrderDate(id, date))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool EditOrderStatus(string id, Status status)
        {
            if(_administratorDataAccess.EditOrderStatus(id, status))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool EditProductCategory(string id, CategoryProduct category)
        {
            if (_administratorDataAccess.EditProductCategory(id, category))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool EditProductDescription(string id, string description)
        {
            if (_administratorDataAccess.EditProductDescription(id, description))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool EditProductPrice(string id, decimal price)
        {
            if (_administratorDataAccess.EditProductPrice(id, price))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Exit()
        {
            _administratorDataAccess.Exit();
        }

        public OrderProduct FillOrder(string id, int bankcard, DateTime date)
        {
            return _administratorDataAccess.FillOrder(id, bankcard, date);
        }

        public Product SearchProduct(string name)
        {
            throw new NotImplementedException();
        }

        public List<Product> ViewAllProducts()
        {
            throw new NotImplementedException();
        }

        public User ViewUserData(string id)
        {
            return _administratorDataAccess.ViewUserData(id);
        }

        public List<User> ViewUsersData()
        {
            return _administratorDataAccess.ViewUsersData();
        }
    }
}
