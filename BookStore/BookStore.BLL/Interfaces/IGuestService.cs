﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Services.Interfaces
{
    interface IGuestService
    {
        bool SignIn(string name, string password);
        void RegisterUser(string email, string name, string password, string confirnPassword);
        void RegisterAdmin(string name, string password, string confirnPassword);
    }
}
