﻿using BookStore.DataAccess.Models;
using System;
using System.Collections.Generic;

namespace BookStore.Services.Interfaces
{
    interface IUserService
    {
        void Exit();
        void CreateOrder(Product product);
        OrderProduct FillOrder(string id, DateTime date);
        bool CancelOrder(string id);
        List<OrderProduct> ViewHistory();
        bool SetStatusRecieved(string id);
        void EditPersonalData(string username, string password, string confirnPassword);
    }
}
