﻿using BookStore.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Services.Interfaces
{
    interface IBaseService
    {
        List<Product> ViewAllProducts();
        Product SearchProduct(string name);
    }
}
