﻿using System;
using System.Linq;
using BookStore.DataAccess.Helpers;
using BookStore.DataAccess.Models;
using BookStore.Services.Implementation;

namespace BookStore
{
    internal static class StoreApi
    {
        static void Main(string[] args)
        {
            var mockContext = new MockContext();
            var userService = new UserService();
            var adminService = new AdministratorService();

            Console.WriteLine("\t\t\t\t\t<<BookStore>>");
            Console.WriteLine("Welcome, dear guest.");
            var user = new User();
            var admin = new Admininstrator();
            bool IsNotSingIn = true;
            while (IsNotSingIn)
            {
                Console.WriteLine("Press 1 - to sign in, Press 2 - to register as user, Press 3 - to register as administrator");
                string choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        {
                            Console.WriteLine("Enter a name");
                            string username = Console.ReadLine();
                            Console.WriteLine("Enter a password");
                            string password = Console.ReadLine();
                            Console.WriteLine("Confirm password");
                            string confirnPassword = Console.ReadLine();
                            if ((mockContext.Users.Exists(u => u.UserName == username && u.Password == password && u.ConfirmPassword == confirnPassword && !u.IsInStore)) && (DateTime.Now.Hour > 4 || DateTime.Now.Hour < 17))
                            {
                                user.UserName = username;
                                user.Password = password;
                                user.ConfirmPassword = confirnPassword;
                                user.IsInStore = true;
                                Console.WriteLine($"Good Day, dear {user.UserName}");
                                IsNotSingIn = false;
                                break;
                            }
                            else if ((mockContext.Users.Exists(u => u.UserName == username && u.Password == password && u.ConfirmPassword == confirnPassword && !u.IsInStore)) && (DateTime.Now.Hour < 4 || DateTime.Now.Hour >= 17))
                            {
                                user.UserName = username;
                                user.Password = password;
                                user.ConfirmPassword = confirnPassword;
                                user.IsInStore = true;
                                Console.WriteLine($"Good evening, dear {user.UserName}");
                                IsNotSingIn = false;
                                break;
                            }
                            if ((mockContext.Admininstrators.Exists(a => a.Name == username && a.Password == password && a.ConfirmPassword == confirnPassword && !a.IsInStore)) && (DateTime.Now.Hour > 4 || DateTime.Now.Hour < 17))
                            {
                                admin.Name = username;
                                admin.Password = password;
                                admin.ConfirmPassword = confirnPassword;
                                admin.IsInStore = true;
                                Console.WriteLine($"Good Day, dear {admin.Name}");
                                IsNotSingIn = false;
                                break;
                            }
                            else if ((mockContext.Admininstrators.Exists(a => a.Name == username && a.Password == password && a.ConfirmPassword == confirnPassword && !a.IsInStore)) && (DateTime.Now.Hour < 4 || DateTime.Now.Hour >= 17))
                            {
                                admin.Name = username;
                                admin.Password = password;
                                admin.ConfirmPassword = confirnPassword;
                                admin.IsInStore = true;
                                Console.WriteLine($"Good evening, dear {admin.Name}");
                                IsNotSingIn = false;
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Not found user or admin, check name and password");
                                break;
                            }
                        }
                    case "2":
                        {
                            Console.WriteLine("Enter an email");
                            string email = Console.ReadLine();
                            Console.WriteLine("Enter a name");
                            string username = Console.ReadLine();
                            Console.WriteLine("Enter a password");
                            string password = Console.ReadLine();
                            Console.WriteLine("Confirm password");
                            string confirnPassword = Console.ReadLine();
                            mockContext.Users.Add(new User() { Email = email, UserName = username, Password = password, ConfirmPassword = confirnPassword });
                            Console.WriteLine($"{username} was registred successfully");
                            break;
                        }
                    case "3":
                        {
                            Console.WriteLine("Enter an administator's name");
                            string name = Console.ReadLine();
                            Console.WriteLine("Enter a password");
                            string password = Console.ReadLine();
                            Console.WriteLine("Confirm password");
                            string confirnPassword = Console.ReadLine();
                            mockContext.Admininstrators.Add(new Admininstrator() { Name = name, Password = password, ConfirmPassword = confirnPassword });
                            Console.WriteLine($"{name} was registred successfully");
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Enter only from 1 to 3 digit");
                            break;
                        }
                }
            }
            if (user.IsInStore)
                while (user.IsInStore)
                {
                    Console.WriteLine("Press 1 - to view all products, Press 2 - to search product by name, Press 3 - to create new order, 4 - to fill order, 5 - to cancel the order, 6 - View history of orders, 7 - To set order status recived, 8 - Edit personal infromation, 9 - To exit from store");
                    string choice = Console.ReadLine();
                    switch (choice)
                    {
                        case "1":
                            {
                                foreach (var product in userService.ViewAllProducts())
                                {
                                    Console.WriteLine($"{product.Name} || {product.CategoryProduct} || {product.Price} || {product.Description}");
                                }
                                break;
                            }
                        case "2":
                            {
                                Console.WriteLine("Enter a product name");
                                string name = Console.ReadLine();
                                var product = userService.SearchProduct(name);
                                Console.WriteLine($"{product.Name} || {product.CategoryProduct} || {product.Price} || {product.Description}");
                                break;
                            }
                        case "3":
                            {
                                Console.WriteLine("Enter a product name");
                                string name = Console.ReadLine();
                                var product = userService.SearchProduct(name);
                                userService.CreateOrder(product);
                                Console.WriteLine($"{product} successfully added to your bucket");
                                break;
                            }
                        case "4":
                            {
                                var orders = user.Orders.Where(o => o.Status == Status.New);
                                foreach(var ord in orders)
                                {
                                    Console.WriteLine($"{ord.Product.Name} || {ord.Status} || {ord.Product.Price}");
                                }
                                Console.WriteLine("Enter an order id");
                                string orderId = Console.ReadLine();
                                var order = mockContext.Orders.First(o => o.Id.ToString() == orderId);
                                Console.WriteLine("Enter your bank card");
                                int bankCard = Convert.ToInt32(Console.ReadLine());
                                var orderProduct = new OrderProduct()
                                {
                                    Order = order,
                                    BankCard = bankCard,
                                    OrderDate = DateTime.Now,
                                    Product = order.Product
                                };
                                mockContext.OrderProducts.Add(orderProduct);
                                break;
                            }
                        case "5":
                            {
                                var orders = mockContext.Orders.Where(o => o.User.IsInStore);
                                foreach (var order in orders)
                                {
                                    Console.WriteLine($"{order.Id} || {order.Product.Name} || {order.Status}");
                                }
                                Console.WriteLine("Enter a order's id");
                                string id = Console.ReadLine();
                                userService.CancelOrder(id);
                                Console.WriteLine($"{id} successfully canceled by user");
                                break;
                            }
                        case "6":
                            {
                                foreach (var item in userService.ViewHistory())
                                {
                                    Console.WriteLine($"{item.Id} || {item.Product.Name} || {item.Product.Price} || {item.Order.User.UserName} || {item.OrderDate}");
                                }
                                break;
                            }
                        case "7":
                            {
                                var orders = mockContext.Orders.Where(o => o.User.IsInStore);
                                foreach (var order in orders)
                                {
                                    Console.WriteLine($"{order.Id} || {order.Product.Name} || {order.Status}");
                                }
                                Console.WriteLine("Enter an order id");
                                string id = Console.ReadLine();
                                userService.SetStatusRecieved(id);
                                Console.WriteLine($"{id} successfully canceled by user");
                                break;
                            }
                        case "8":
                            {
                                if (user.IsInStore)
                                {
                                    Console.WriteLine("Enter an email");
                                    string email = Console.ReadLine();
                                    Console.WriteLine("Enter a name");
                                    string username = Console.ReadLine();
                                    Console.WriteLine("Enter a password");
                                    string password = Console.ReadLine();
                                    Console.WriteLine("Confirm password");
                                    string confirnPassword = Console.ReadLine();
                                    Console.WriteLine("Enter a name");
                                    user.Email = email;
                                    user.UserName = username;
                                    user.Password = password;
                                    user.ConfirmPassword = confirnPassword;
                                }
                                else if (admin.IsInStore)
                                {
                                    Console.WriteLine("Enter a name");
                                    string username = Console.ReadLine();
                                    Console.WriteLine("Enter a password");
                                    string password = Console.ReadLine();
                                    Console.WriteLine("Confirm password");
                                    string confirnPassword = Console.ReadLine();
                                    admin.Name = username;
                                    admin.Password = password;
                                    admin.ConfirmPassword = confirnPassword;
                                }
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("Enter only from 1 to 8 digit");
                                break;
                            }

                    }
                }
            if (admin.IsInStore)
            {
                while (admin.IsInStore)
                {
                    Console.WriteLine("Press 1 - to view all products, Press 2 - to search product by name, Press 3 - to create new order, 4 - to fill order, 5 - View user personal data, 6 - Edit user personal data, 7 - To create new product, 8 - Edit exists product, 9 - Edit order status, 0 - Exit from store");
                    string choice = Console.ReadLine();
                    switch (choice)
                    {

                        case "1":
                            {
                                foreach (var product in userService.ViewAllProducts())
                                {
                                    Console.WriteLine($"{product.Name} || {product.CategoryProduct} || {product.Price} || {product.Description}");
                                }
                                break;
                            }
                        case "2":
                            {
                                Console.WriteLine("Enter a product name");
                                string name = Console.ReadLine();
                                var product = userService.SearchProduct(name);
                                Console.WriteLine($"{product.Name} || {product.CategoryProduct} || {product.Price} || {product.Description}");
                                break;
                            }
                        case "3":
                            {
                                Console.WriteLine("Enter a product name");
                                string name = Console.ReadLine();
                                var product = userService.SearchProduct(name);
                                userService.CreateOrder(product);
                                Console.WriteLine($"{product} successfully added to your bucket");
                                break;
                            }
                        case "4":
                            {
                                var orders = user.Orders.Where(o => o.Status == Status.New);
                                foreach (var ord in orders)
                                {
                                    Console.WriteLine($"{ord.Product.Name} || {ord.Status} || {ord.Product.Price}");
                                }
                                Console.WriteLine("Enter an order id");
                                string orderId = Console.ReadLine();
                                Console.WriteLine("Enter your bank card");
                                int bankCard = Convert.ToInt32(Console.ReadLine());
                                adminService.FillOrder(orderId,bankCard,DateTime.Now);
                                break;
                            }
                        case "5":
                            {   
                                Console.WriteLine("Enter user id");
                                string id = Console.ReadLine();
                                var userId = adminService.ViewUserData(id);
                                Console.WriteLine($"{userId.Id} || {userId.UserName} || {userId.Password} || {userId.ConfirmPassword} || {userId.Email}");
                                break;
                            }
                        case "6":
                            {
                                Console.WriteLine("Enter user id");
                                string id = Console.ReadLine();
                                Console.WriteLine("Enter an email");
                                string email = Console.ReadLine();
                                Console.WriteLine("Enter a name");
                                string username = Console.ReadLine();
                                Console.WriteLine("Enter a password");
                                string password = Console.ReadLine();
                                Console.WriteLine("Confirm password");
                                string confirnPassword = Console.ReadLine();
                                var userId = adminService.ViewUserData(id);
                                userId.Email = email;
                                userId.UserName = username;
                                userId.Password = password;
                                userId.ConfirmPassword = confirnPassword;
                                break;
                            }
                        case "7":
                            {
                                Console.WriteLine("Enter a product name");
                                string name = Console.ReadLine();
                                Console.WriteLine("Enter a product category, 1 - Action, 2 - Platformer, 3 - Arcade, 0 - Strategy");
                                string cat = Console.ReadLine();
                                CategoryProduct category = new CategoryProduct();
                                if (cat == "1")
                                {
                                    category = CategoryProduct.Action;
                                }
                                if (cat == "2")
                                {
                                    category = CategoryProduct.Platformer;
                                }
                                if (cat == "3")
                                {
                                    category = CategoryProduct.Arcade;
                                }
                                if (cat == "0")
                                {
                                    category = CategoryProduct.Strategy;
                                }
                                Console.WriteLine("Enter a product description");
                                string description = Console.ReadLine();
                                Console.WriteLine("Enter a product price");
                                decimal price = Convert.ToDecimal(Console.ReadLine());
                                adminService.CreateProduct(name, category, description, price);
                                break;
                            }
                        case "8":
                            {
                                Console.WriteLine("Enter a product id");
                                string id = Console.ReadLine();
                                Console.WriteLine("Enter a product name");
                                string name = Console.ReadLine();
                                Console.WriteLine("Enter a product category, 1 - Action, 2 - Platformer, 3 - Arcade, 0 - Strategy");
                                string cat = Console.ReadLine();
                                CategoryProduct category = new CategoryProduct();
                                if (cat == "1")
                                {
                                    category = CategoryProduct.Action;
                                }
                                if (cat == "2")
                                {
                                    category = CategoryProduct.Platformer;
                                }
                                if (cat == "3")
                                {
                                    category = CategoryProduct.Arcade;
                                }
                                if (cat == "0")
                                {
                                    category = CategoryProduct.Strategy;
                                }
                                Console.WriteLine("Enter a product description");
                                string description = Console.ReadLine();
                                Console.WriteLine("Enter a product price");
                                decimal price = Convert.ToDecimal(Console.ReadLine());
                                adminService.EditProductCategory(id, category);
                                adminService.EditProductDescription(id, description);
                                adminService.EditProductPrice(id, price);

                                break;
                            }
                        case "9":
                            {
                                Console.WriteLine("Enter a order's id");
                                string id = Console.ReadLine();
                                var order = mockContext.Orders.First(o => o.Id.ToString() == id);
                                Console.WriteLine($"Order has{order.Status}, enter status order, 1 - AdminCanceled, 2 - Finished, 3 - New, 4 - Paid, 5 - Recived, 6 - Sent, 7 - UserCanceled");
                                string st = Console.ReadLine();
                                Status status = new Status();
                                if (st == "1")
                                {
                                    status = Status.AdminCanceled;
                                }
                                if (st == "2")
                                {
                                    status = Status.Finished;
                                }
                                if (st == "3")
                                {
                                    status = Status.New;
                                }
                                if (st == "4")
                                {
                                    status = Status.Paid;
                                }
                                if (st == "5")
                                {
                                    status = Status.Recieved;
                                }
                                if (st == "6")
                                {
                                    status = Status.Sent;
                                }
                                if (st == "7")
                                {
                                    status = Status.UserCanceled;
                                }
                                order.Status = status;

                                break;

                            }
                        default:
                            {
                                Console.WriteLine("Enter only from 1 to 8 digit");
                                break;
                            }
                    }

                }
            }
        }
    }
}
