﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.BuisnessLogic.Services.Interfaces
{
    interface IAdministrator
    {
        Task AddCategory(CategoryDTO categoryDTO);
    }
}
