﻿using BookStore.BuisnessLogic.DTO.Abstractions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BookStore.BuisnessLogic.DTO
{
    public class AdministratorDto : BaseEntityDto
    {
        public string Name { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [StringLength(16, MinimumLength = 6, ErrorMessage = "Invalid password length minimum 6 symbol")]
        [RegularExpression(@"[a–z]\[0-9]\S$", ErrorMessage = "Administrator name must include only a-z symbols")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Passwords are not the same")]
        [Compare("Password", ErrorMessage = "Passwords are not the same")]
        public string ConfirmPassword { get; set; }
    }
}
