﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BuisnessLogic.DTO.Abstractions
{
    public class BaseEntityDto
    {
        public Guid Id { get; set; }
    }
}
