﻿using BookStore.BuisnessLogic.DTO.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BuisnessLogic.DTO
{
    public class ProductDto : BaseEntityDto
    {
        public string Name { get; set; }
        public CategoryProduct CategoryProduct { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public List<ProductDto> productDtos { get; set; }

    }
}
