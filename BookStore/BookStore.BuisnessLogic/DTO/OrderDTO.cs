﻿using BookStore.BuisnessLogic.DTO.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BuisnessLogic.DTO
{
    public class OrderDto : BaseEntityDto
    {
        public ProductDto ProductDto { get; set; }
        public int ProductId { get; set; }
        public bool? IsNew { get; set; }
        public bool? IsAdminCanceled { get; set; }
        public bool? IsUserCanceled { get; set; }
        public bool? IsPaid { get; set; }
        public bool? IsSent { get; set; }
        public bool? IsRecieved { get; set; }
        public bool? IsFinished { get; set; }
    }
}
